package controllers;

import java.util.List;

import models.Blank;
import net.sf.oval.guard.Post;
import play.data.validation.Required;
import play.mvc.Controller;

public class Application extends Controller {

    public static void index() {
    	String activeTab = "allForms";
    	List<Blank> blanks = Blank.find(
    	            "order by createDate desc"
    	        ).from(0).fetch();
        render(blanks, activeTab);
    }
    
    public static void searchIin(String iin) {
    	String activeTab = "search";
    	if(iin==null || iin.isEmpty()) {
    		render(activeTab, iin);
    	}
    	else {
        	List<Blank> blanks = Blank.find(
        	            "iin LIKE ? " +
        	            "order by createDate desc",
        	            "%" + iin + "%"
        	        ).from(0).fetch();
        	render(blanks, activeTab, iin);
    	}
    }

    public static void searchFio(String fio) {
    	String activeTab = "search";
    	if(fio==null || fio.isEmpty()) {
    		render(activeTab, fio);
    	}
    	else {
    		
        	List<Blank> blanks = Blank.find(
        	            "UPPER(fio) LIKE UPPER(?) " +
        	            "order by createDate desc",
        	            "%" + fio + "%"
        	        ).from(0).fetch();
        	render(blanks, activeTab, fio);
    	}
    }
    
    public static void searchBarCode(String barCode) {
    	String activeTab = "search";
    	if(barCode==null || barCode.isEmpty()) {
    		render(activeTab, barCode);
    	}
    	else {
    		
        	List<Blank> blanks = Blank.find(
        	            "order by createDate desc"
        	        ).from(0).fetch(1);
        	render(blanks, activeTab, barCode);
    	}
    }
}