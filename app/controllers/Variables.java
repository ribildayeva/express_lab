package controllers;

import java.util.List;

import models.Variable;
import play.mvc.Controller;

public class Variables extends Controller {

    public static void blank(String id) {
    	List<Variable> variables = Variable.find(
    		    "select v from Variable v " +
    		    "where v.blank.id = ?", Long.valueOf(id)).fetch();
    	System.out.println("vari:" + variables.size());
    	render(variables);
    }

}