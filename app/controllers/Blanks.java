package controllers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import models.Blank;
import models.Variable;

import org.apache.commons.io.IOUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import play.Logger;
import play.db.jpa.JPA;
import play.libs.XPath;
import play.templates.Template;
import play.templates.TemplateLoader;

public class Blanks extends Application {

	public static void conservice(){
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			factory.setNamespaceAware(true);
			DocumentBuilder dbuilder = factory.newDocumentBuilder();
			Document doc = dbuilder.parse(request.body);
			String iin = XPath.selectNode("//iin", doc).getTextContent();
			String service = XPath.selectNode("//service", doc).getTextContent();
			Logger.info("params: %s and %s",iin,service);
			List<Blank> blanks = JPA.em().createQuery("" +
	    			" from " +
	    			"	Blank b join fetch b.variableList list" +
	    			" where " +
	    			"	b.iin = :iin " +
	    			"	and b.serviceCode = :serviceCode " +
	    			" order by" +
	    			"	b.createDate desc", Blank.class)
				.setParameter("iin",iin)
				.setParameter("serviceCode", service)
				.getResultList();
	    	if(blanks.size()>0){
	    		Blank blank = blanks.get(0);
	    		Map<String,Object> map = new HashMap<String, Object>();
	    		map.put("blank",blank);
	    		Template template = TemplateLoader.load("Blanks/conservice.xml");
		    	renderXml(template.render(map));
	    	}
		} catch (Exception e) {
			e.printStackTrace();
		}
		Template template = TemplateLoader.load("Blanks/emptyconservice.xml");
    	renderXml(template.render());
	}

	public static void pensXml(File sendfile) {
		Logger.info("file uploading: %s", sendfile.getName());
		try {
			FileInputStream fis = new FileInputStream(sendfile);
			
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			factory.setNamespaceAware(true);
			DocumentBuilder dbuilder = factory.newDocumentBuilder();
			Document doc = dbuilder.parse(fis);

			String appname = "";
//			String referenceTypeKey = "";
//			String referenceTypeValue = "";
			NodeList header = doc.getElementsByTagName("header");
			if (header.item(0) != null) {
				Node appName = XPath.selectNode("//appname", header.item(0));
				if (appName != null) {
					String[] app = appName.getTextContent().split(";");
					appname = app[0];
//					String[] referenceType = app[1].split("=");
//					referenceTypeKey = referenceType[0];
//					referenceTypeValue = referenceType[1];
				}
			}

			Blank blank = new Blank();
			blank.serviceCode = appname;
			blank.createDate = new Date();
			//Variable variableRef = new Variable();
			//variableRef.key = referenceTypeKey;
			//variableRef.value = referenceTypeValue;
			//variableRef.blank = blank;
			//blank.variableList.add(variableRef);

			NodeList pages = doc.getElementsByTagName("page");
			for (int i = 0; i < pages.getLength(); i++) {
				NodeList variables = pages.item(i).getChildNodes();
				for (int j = 0; j < variables.getLength(); j++) {
					String key = variables.item(j).getLocalName();
					String value = variables.item(j).getTextContent();
					if ((key != null) && (!key.isEmpty())) {
						System.out.println(key + " - " + value);
						Variable variable = new Variable();
						variable.key = key;
						variable.value = value;
						variable.blank = blank;
						if (key.equals("i_iin")) {
							blank.iin = value;
						}
						if (key.equals("fio")) {
							blank.fio = value;
						}
						blank.variableList.add(variable);
					}
				}
			}
			boolean isCreated = blank.create();
			render(blank);
			Logger.info("Blank created: %s", isCreated);
		} catch (Exception e) {
			e.printStackTrace();
		}
		render();
	}
	
	public static void pensXmlPdf(File sendfile, File sendfile2) {
		Logger.info("file uploading: %s", sendfile.getName());
		Logger.info("file2 uploading: %s", sendfile2.getName());
		try {
			FileInputStream fis = new FileInputStream(sendfile2);
			FileInputStream fis1 = new FileInputStream(sendfile2);
			FileInputStream fis2 = new FileInputStream(sendfile);

			String filePath1 = "public/upload/" + sendfile.getName();
			String filePath2 = "public/upload/" + sendfile2.getName();
			
			FileOutputStream fout1 = new FileOutputStream(filePath1);
			FileOutputStream fout2 = new FileOutputStream(filePath2);
			
			IOUtils.copy(fis1, fout1);
			IOUtils.copy(fis2, fout2);

			fout1.flush(); fout1.close();
			fout2.flush(); fout2.close();
			
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			factory.setNamespaceAware(true);
			DocumentBuilder dbuilder = factory.newDocumentBuilder();
			Document doc = dbuilder.parse(fis);

			String appname = "";
//			String referenceTypeKey = "";
//			String referenceTypeValue = "";
			NodeList header = doc.getElementsByTagName("header");
			if (header.item(0) != null) {
				Node appName = XPath.selectNode("//appname", header.item(0));
				if (appName != null) {
					String[] app = appName.getTextContent().split(";");
					appname = app[0];
//					String[] referenceType = app[1].split("=");
//					referenceTypeKey = referenceType[0];
//					referenceTypeValue = referenceType[1];
				}
			}

			Blank blank = new Blank();
			blank.serviceCode = appname;
			blank.createDate = new Date();
			blank.pdfUrl = filePath2;
			//Variable variableRef = new Variable();
			//variableRef.key = referenceTypeKey;
			//variableRef.value = referenceTypeValue;
			//variableRef.blank = blank;
			//blank.variableList.add(variableRef);

			NodeList pages = doc.getElementsByTagName("page");
			for (int i = 0; i < pages.getLength(); i++) {
				NodeList variables = pages.item(i).getChildNodes();
				for (int j = 0; j < variables.getLength(); j++) {
					String key = variables.item(j).getLocalName();
					String value = variables.item(j).getTextContent();
					if ((key != null) && (!key.isEmpty())) {
						System.out.println(key + " - " + value);
						Variable variable = new Variable();
						variable.key = key;
						variable.value = value;
						variable.blank = blank;
						if (key.equals("i_iin")) {
							blank.iin = value;
						}
						if (key.equals("fio")) {
							blank.fio = value;
						}
						blank.variableList.add(variable);
					}
				}
			}
			boolean isCreated = blank.create();
			render(blank);
			Logger.info("Blank created: %s", isCreated);
		} catch (Exception e) {
			e.printStackTrace();
		}
		render();
	}
	
	
	public static void pensTest(File sendfile) {
		try {
			Logger.info("file uploading: %s", sendfile.getName());
			FileInputStream fis = new FileInputStream(sendfile);
			
			String fileName = sendfile.getName();
			
			StringTokenizer st = new StringTokenizer(fileName, ".");
			String formName = st.nextToken();
			String fileExtension = st.nextToken();
	
			Logger.info("fileName: %s", fileName);
			Logger.info("formName: %s", formName);
			Logger.info("fileExtension: %s", fileExtension);
	
			// Поиск бланка или создается новый
			Blank blank = Blank.find(
		            "formName = ?",
		            formName
		        ).first();
			
			if(blank==null) {
				Logger.info("New Blank is created");
				blank = new Blank();
				blank.formName = formName;
				blank.createDate = new Date();
				blank.barCode = "5900861242623";
				blank.create();
			}
			else {
				Logger.info("Blank is taken with name " + blank.formName);
			}
			
			// Если пришла Xml
			if(fileExtension.equals("xml")) {
				
				DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
				factory.setNamespaceAware(true);
				DocumentBuilder dbuilder = factory.newDocumentBuilder();
				Document doc = dbuilder.parse(fis);

				String appname = "";
				NodeList header = doc.getElementsByTagName("header");
				if (header.item(0) != null) {
					Node appName = XPath.selectNode("//appname", header.item(0));
					if (appName != null) {
						String[] app = appName.getTextContent().split(";");
						appname = app[0];
					}
				}

				blank.serviceCode = appname;

				NodeList pages = doc.getElementsByTagName("page");
				for (int i = 0; i < pages.getLength(); i++) {
					NodeList variables = pages.item(i).getChildNodes();
					for (int j = 0; j < variables.getLength(); j++) {
						String key = variables.item(j).getLocalName();
						String value = variables.item(j).getTextContent();
						if ((key != null) && (!key.isEmpty())) {
							Logger.info(key + " - " + value);
							Variable variable = new Variable();
							variable.key = key;
							variable.value = value;
							variable.blank = blank;
							if (key.equals("i_iin")) {
								blank.iin = value;
							}
							if (key.equals("fio")) {
								blank.fio = value;
							}
							if (key.equals("name") || key.equals("surname")) {
								blank.fio = blank.fio==null?"":blank.fio + value + " ";
								//blank.fio += value + " ";
							}
						
							blank.variableList.add(variable);
						}
					}
				}
				blank.save();
			}
			// Если пришла Pdf
			else {
				String filePath = "public/upload/" + fileName;

				Logger.info("filePath: " + filePath);
				FileOutputStream fout = new FileOutputStream(filePath);
				
				IOUtils.copy(fis, fout);

				blank.pdfUrl = filePath;
				blank.save();
				
				fout.flush(); 
				fout.close();
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}

		renderHtml("done");
	}

}
