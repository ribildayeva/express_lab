/*
 * JBoss, Home of Professional Open Source
 * Copyright 2013, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import play.db.jpa.GenericModel;

@Entity
@Table(name = "b_blank")
public class Blank extends GenericModel implements Serializable {
    /** Default value included to remove warning. Remove or modify at will. **/
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    public Long id;

    @Column(name="SERVICE_CODE_")
    public String serviceCode;

    @Column(name="CREATE_")
    public Date createDate;

    @Column(name="IIN_")
    public String iin;

    @Column(name="FILE_NAME_")
    public String formName;
    
    @Column(name="BAR_CODE_")
    public String barCode;
    
    @Column(name="FIO_")
    public String fio;
    
    @Column(name="PDF_")
    public String pdfUrl;

    @OneToMany(mappedBy="blank", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    public List<Variable> variableList = new ArrayList<Variable>(); 
}