package models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import play.db.jpa.GenericModel;

@Entity
@Table(name = "b_variable")
public class Variable extends GenericModel implements Serializable{
	
	@Id
    @GeneratedValue
	@Column(name="ID_")
	public Long id;
	
	@ManyToOne(fetch = FetchType.LAZY) 
	@JoinColumn(name="BLANK_")
	public Blank blank;
	
	@Column(name="KEY_")
	public String key;
	
	@Column(name="VALUE_",columnDefinition="TEXT")
	public String value;
}